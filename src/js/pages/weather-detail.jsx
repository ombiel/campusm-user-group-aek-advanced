var React = require("-aek/react");
var {Listview} = require("-components/listview");
var {BasicSegment} = require("-components/segment");
var Page = require("-components/page");
var store = require("../data/store");
var _ = require("-aek/utils");


var WeatherDetailPage = React.createClass({

  render:function() {

    let city = this.props.ctx.params.city;
    let data = _.find(store.getState().weather.items,{city});
    let items = [];

    if(data) {
      items = {
        City:city,
        Summary:data.text,
        "Wind Chill":data.wind.chill,
        "Humidity":data.atmosphere.humidity
      };
    }


    return (
      <Page>
        <BasicSegment>
          <Listview items={items}/>
        </BasicSegment>
      </Page>
    );

  }

});

module.exports = WeatherDetailPage;
