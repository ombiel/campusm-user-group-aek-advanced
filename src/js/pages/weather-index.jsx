var React = require("-aek/react");
var {Listview} = require("-components/listview");
var {BasicSegment} = require("-components/segment");
var Page = require("-components/page");
var store = require("../data/store");




var WeatherIndexPage = React.createClass({

  onClick(item,ev) {
    ev.preventDefault();
    this.props.ctx.router.goto(`weather/detail/${item.location.city}`);
  },

  render:function() {

    let {items,loading} = store.getState().weather;

    return (
      <Page>
        <BasicSegment loading={loading}>
          <Listview items={items} onClick={this.onClick}/>
        </BasicSegment>
      </Page>
    );

  }

});

module.exports = WeatherIndexPage;
