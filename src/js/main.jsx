var React = require("-aek/react");
var Container = require("-components/container");
var {VBox,Panel} = require("-components/layout");
var {BannerHeader} = require("-components/header");
var request = require("-aek/request");

var {RouterView} = require("-components/router");
var WeatherIndexPage = require("./pages/weather-index");
var WeatherDetailPage = require("./pages/weather-detail");

var store = require("./data/store");

var Screen = React.createClass({

  componentDidMount() {

    store.subscribe(()=>{
      this.forceUpdate();
    });


    request.get("http://query.yahooapis.com/v1/public/yql/exlibris/weather-multi?format=json")
    .end((err,res)=> {

      let data = res.body.query.results.channel.map(({location,item,wind,atmosphere,astronomy})=>{
        let forecast = item.forecast[0];
        return {
          heading:location.city,
          text:`${forecast.text} ${forecast.high}°C`,
          city:location.city,
          location,
          wind,
          atmosphere,
          astronomy
        };
      });

      store.dispatch({type:"SET_ITEMS",payload:data});

    });
  },

  render:function() {

    return (
      <Container>
        <VBox>
          <BannerHeader theme="alt" key="header" flex={0}>My Screen</BannerHeader>
          <Panel>
            <RouterView>
              <WeatherIndexPage path="/" />
              <WeatherDetailPage path="/weather/detail/:city" />
            </RouterView>
          </Panel>
        </VBox>
      </Container>
    );

  }

});

React.render(<Screen/>,document.body);
