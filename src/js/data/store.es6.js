// var _ = require("-aek/utils");
var aekFluxStore = require("-aek/flux-store");

var reducers = {
  weather:(state = {$$$loading:true},action)=>{
    switch(action.type) {
      case "SET_ITEMS":
        state = {$$$loading:false,items:action.payload};
        break;
    }
    return state;
  }
};

var localStorageKey = "aek-examples/user-group";

module.exports = aekFluxStore({reducers,localStorageKey});
